var p = new Player();
var f = new Food();

function Player(){
	this.x = Math.floor(Math.random()*20)*20;
	this.y = Math.floor(Math.random()*20)*20;
	this.gauge;
	this.show = function(){
		fill(p.gauge,0,0,255);
		rect(this.x,this.y,20,20);
	}
}

function gaugeCalculate(px,py,fx,fy){
	var dist = distCalculate(px,py,fx,fy)/20;
	return Math.floor(255/(dist+1));
}

function distCalculate(px,py,fx,fy){
	return Math.hypot(px-fx, py-fy);
}

function Food(){
	this.x = Math.floor(Math.random()*20)*20;
	this.y = Math.floor(Math.random()*20)*20;
	this.show = function(){
		fill(255);
		rect(this.x,this.y,20,20);
	}
}

function setup(){
	createCanvas(400,400);
}

function draw(){
	background(51);
	p.gauge = gaugeCalculate(p.x,p.y,f.x,f.y);
	//f.show();
	p.show();
}

function keyPressed(){
	if (keyCode === 32){ //Spacebar
		if(p.gauge == 255){
			f.x = Math.floor(Math.random()*20)*20;
			f.y = Math.floor(Math.random()*20)*20;
		}
	}
	if (keyCode === RIGHT_ARROW && p.x <= 360){
		p.x = p.x+20;
	} else if (keyCode === LEFT_ARROW && p.x >= 20){
		p.x = p.x-20;
	} else if (keyCode === UP_ARROW && p.y >= 20){
		p.y = p.y-20;
	} else if (keyCode === DOWN_ARROW && p.y <= 360){
		p.y = p.y+20;
	}
}